#pragma once

#include <QObject>

#include "MyAdapters.hpp"

/// register short names for the custom types (to use like an enum), can have aliases (yes the QML type has to be repeated...) :

QML_DECLARE_STRONG_TYPE (String,   QString)
QML_DECLARE_STRONG_TYPE (Boolean,  bool)
QML_DECLARE_STRONG_TYPE (Integer,  int)
QML_DECLARE_STRONG_TYPE (Decimal,  qreal)
QML_DECLARE_STRONG_TYPE (DateTime, QString)
QML_DECLARE_STRONG_TYPE (Country,  int)
QML_DECLARE_STRONG_TYPE (Weekday,  int)
QML_DECLARE_STRONG_TYPE (Duration, Duration)
QML_DECLARE_STRONG_TYPE (List,     QJsonArray)

QML_DERIVED_STRONG_TYPE (Email,    String)
QML_DERIVED_STRONG_TYPE (Phone,    String)
QML_DERIVED_STRONG_TYPE (Guid,     String)
QML_DERIVED_STRONG_TYPE (Ref,      Guid)
QML_DERIVED_STRONG_TYPE (Percent,  Decimal)

struct FORCE_MOC_EXEC_TYPE_DEFS { Q_GADGET };
