import qbs;

Project {
    name: "QtQmlModularTypeSystem";
    minimumQbsVersion: "1.7.0";
    references: [];

    Product {
        name: "app-QtQmlModularTypeSystem";
        type: "application";
        targetName: "QtQmlModularTypeSystem";
        cpp.rpaths: ["$ORIGIN", "$ORIGIN/lib"];
        cpp.includePaths: [sourceDirectory];
        cpp.cxxLanguageVersion: "c++17";
        Qt.core.resourcePrefix: "/";
        Qt.core.resourceSourceBase: sourceDirectory;
        Qt.core.resourceFileBaseName: "data_qmlfiles";
        Qt.qml.importName: "QtQmlModularTypeSystem";
        Qt.qml.importVersion: "1";

        Depends {
            name: "cpp";
        }
        Depends {
            name: "Qt";
            submodules: ["core", "gui", "network", "qml", "quick"];
        }
        Group {
            name: "C++ files";
            files: [
                "MyAdapters.hpp",
                "MyDataBase.hpp",
                "MyModeldata.hpp",
                "MyTypeDefs.hpp",
                "QtQmlModularTypeSystem.hpp",
                "main.cpp",
                "main.hpp",
            ]
        }
        Group {
            name: "QML Files";
            fileTags: "qt.core.resource_data";
            files: [
                "ui.qml",
            ];
        }
        Group {
            qbs.install: true;
            fileTagsFilter: product.type;
        }
    }
}
