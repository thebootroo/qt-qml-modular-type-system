import QtQuick 2.1;
import QtQuick.Window 2.1;
import QtQmlModularTypeSystem 1.0;
import MyTestApp 1.0;

Window {
    id: window;
    width: 800;
    height: 600;
    visible: true;
    Component.onCompleted: {
        console.log (JSON.stringify (BDD.contact_emailAddr.name),   JSON.stringify (BDD.contact_emailAddr.typeLine));
        console.log (JSON.stringify (BDD.contact_nicknames.name),   JSON.stringify (BDD.contact_nicknames.typeLine));
        console.log (JSON.stringify (BDD.contact_nationality.name), JSON.stringify (BDD.contact_nationality.typeLine));

        console.log ("TST.nationality:", JSON.stringify (TST.nationality.field.typeLine));

        TST.firstname.newValue = "bar";
        console.log ("TST.firstname:", JSON.stringify (TST.firstname.newValue), JSON.stringify (TST.firstname.oldValue));

        TST.nicknames.newValue = ["fooo", "the Foo", "bleh"];
        console.log ("TST.nicknames:", JSON.stringify (TST.nicknames.newValue), JSON.stringify (TST.nicknames.oldValue));

        TST.seniority.newValue.amount = 123;
        TST.seniority.newValue.type   = Duration.Y;
        console.log ("TST.nicknames:", JSON.stringify (TST.nicknames.newValue), JSON.stringify (TST.nicknames.oldValue));
    }

    property StringWrapper str: TST.email;

    Column {
        spacing: 20;
        anchors.centerIn: parent;

        Repeater {
            model: TST.wrappersList;
            delegate: Row {
                id: _entry;
                spacing: 20;

                readonly property WrapperDef qtObject: modelData;

                //Text {
                //    text: _entry.qtObject.name;
                //}
                Text {
                    text: _entry.qtObject.field.name;
                }
                Text {
                    text: _entry.qtObject.field.typeName;
                }
                Text {
                    text: typeof (_entry.qtObject.newValue);
                }
                Text {
                    text: JSON.stringify (_entry.qtObject.newValue);
                }
                Text {
                    text: JSON.stringify (_entry.qtObject.oldValue);
                }
            }
        }
    }
}
