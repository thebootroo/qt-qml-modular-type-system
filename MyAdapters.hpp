#pragma once

#include <QObject>
#include <QMetaEnum>
#include <QJsonArray>
#include <QJsonDocument>

#include <QRectF>

#include "QtQmlModularTypeSystem.hpp"

/// implement handlers for each type that need different C++, QML, JSON, or SQL representation :

struct StringTypeDef : public TypeDef<QString> {
    static cpp_ret_type fallback (void) { return QString { }; }

    static const QString & sqlType (void) { static const QString ret { QS ("TEXT") }; return ret; }

    static qml_ret_type toQmlJsVar   (cpp_arg_type arg) { return arg; }
    static QJsonValue   toJsonValue  (cpp_arg_type arg) { return arg; }
    static QVariant     toSqlVariant (cpp_arg_type arg) { return arg; }

    static cpp_ret_type fromQmlJsVar   (qml_arg_type       arg) { return arg; }
    static cpp_ret_type fromJsonValue  (const QJsonValue & arg) { return arg.toString (); }
    static cpp_ret_type fromSqlVariant (const QVariant &   arg) { return arg.toString (); }
};

struct DecimalTypeDef : public TypeDef<qreal> {
    static cpp_ret_type fallback (void) { return 0.0; }

    static const QString & sqlType (void) { static const QString ret { QS ("REAL") }; return ret; }

    static qml_ret_type toQmlJsVar   (cpp_arg_type arg) { return arg; }
    static QJsonValue   toJsonValue  (cpp_arg_type arg) { return arg; }
    static QVariant     toSqlVariant (cpp_arg_type arg) { return arg; }

    static cpp_ret_type fromQmlJsVar   (qml_arg_type       arg) { return arg; }
    static cpp_ret_type fromJsonValue  (const QJsonValue & arg) { return arg.toDouble (); }
    static cpp_ret_type fromSqlVariant (const QVariant &   arg) { return arg.toReal (); }
};

struct IntegerTypeDef : public TypeDef<int> {
    static cpp_ret_type fallback (void) { return 0; }

    static const QString & sqlType (void) { static const QString ret { QS ("INTEGER") }; return ret; }

    static qml_ret_type toQmlJsVar   (cpp_arg_type arg) { return arg; }
    static QJsonValue   toJsonValue  (cpp_arg_type arg) { return arg; }
    static QVariant     toSqlVariant (cpp_arg_type arg) { return arg; }

    static cpp_ret_type fromQmlJsVar   (qml_arg_type       arg) { return arg; }
    static cpp_ret_type fromJsonValue  (const QJsonValue & arg) { return arg.toInt (); }
    static cpp_ret_type fromSqlVariant (const QVariant &   arg) { return arg.toInt (); }
};

struct BooleanTypeDef : public TypeDef<bool> {
    static cpp_ret_type fallback (void) { return false; }

    static const QString & sqlType (void) { static const QString ret { QS ("INTEGER") }; return ret; }

    static qml_ret_type toQmlJsVar   (cpp_arg_type arg) { return arg; }
    static QJsonValue   toJsonValue  (cpp_arg_type arg) { return arg; }
    static QVariant     toSqlVariant (cpp_arg_type arg) { return (arg ? 1 : 0); }

    static cpp_ret_type fromQmlJsVar   (qml_arg_type       arg) { return arg; }
    static cpp_ret_type fromJsonValue  (const QJsonValue & arg) { return arg.toBool (); }
    static cpp_ret_type fromSqlVariant (const QVariant &   arg) { return (arg.toInt () == 1); }
};

struct DateTimeTypeDef : public TypeDef<QDateTime, QString> {
    static cpp_ret_type fallback (void) { return QDateTime { }; }

    static const QString & sqlType (void) { static const QString ret { QS ("TEXT") }; return ret; }

    static const QString format (void) { static const QString ret { QS ("yyyy-MM-dd hh:mm") }; return ret; }

    static inline QString stringify (cpp_arg_type arg) {
        return arg.toString (format ());
    }

    static inline cpp_ret_type parse (const QString & arg) {
        return QDateTime::fromString (arg.left (19), format ());
    }

    static qml_ret_type toQmlJsVar   (cpp_arg_type arg) { return stringify (arg); }
    static QJsonValue   toJsonValue  (cpp_arg_type arg) { return stringify (arg); }
    static QVariant     toSqlVariant (cpp_arg_type arg) { return stringify (arg); }

    static cpp_ret_type fromQmlJsVar   (qml_arg_type       arg) { return parse (arg); }
    static cpp_ret_type fromJsonValue  (const QJsonValue & arg) { return parse (arg.toString ()); }
    static cpp_ret_type fromSqlVariant (const QVariant &   arg) { return parse (arg.toString ()); }
};

struct DateTypeDef : public TypeDef<QDate, QString> {
    static cpp_ret_type fallback (void) { return QDate { }; }

    static const QString & sqlType (void) { static const QString ret { QS ("TEXT") }; return ret; }

    static const QString format (void) { static const QString ret { QS ("yyyy-MM-dd") }; return ret; }

    static inline QString stringify (cpp_arg_type arg) {
        return arg.toString (format ());
    }

    static inline cpp_ret_type parse (const QString & arg) {
        return QDate::fromString (arg.left (10), format ());
    }

    static qml_ret_type toQmlJsVar   (cpp_arg_type arg) { return stringify (arg); }
    static QJsonValue   toJsonValue  (cpp_arg_type arg) { return stringify (arg); }
    static QVariant     toSqlVariant (cpp_arg_type arg) { return stringify (arg); }

    static cpp_ret_type fromQmlJsVar   (qml_arg_type       arg) { return parse (arg); }
    static cpp_ret_type fromJsonValue  (const QJsonValue & arg) { return parse (arg.toString ()); }
    static cpp_ret_type fromSqlVariant (const QVariant &   arg) { return parse (arg.toString ()); }
};

struct ListTypeDef : public TypeDef<QJsonArray> {
    static QJsonArray fallback (void) { return QJsonArray { }; }

    static const QString & sqlType (void) { static const QString ret { QS ("TEXT") }; return ret; }

    static inline QJsonArray parse (const QString & arg) {
        return QJsonDocument::fromJson (arg.toUtf8 ()).array ();
    }

    static inline QString stringify (const QJsonArray & arg) {
        return QString::fromUtf8 (QJsonDocument { arg }.toJson (QJsonDocument::Compact));
    }

    static qml_ret_type toQmlJsVar   (cpp_arg_type arg) { return arg; }
    static QJsonValue   toJsonValue  (cpp_arg_type arg) { return stringify (arg); }
    static QVariant     toSqlVariant (cpp_arg_type arg) { return stringify (arg); }

    static cpp_ret_type fromQmlJsVar   (qml_arg_type       arg) { return arg; }
    static cpp_ret_type fromJsonValue  (const QJsonValue & arg) { return parse (arg.toString ()); }
    static cpp_ret_type fromSqlVariant (const QVariant &   arg) { return parse (arg.toString ()); }
};

struct Duration {
    Q_GADGET
    QML_NAMED_ELEMENT (Duration)
    QML_UNCREATABLE ("Value object !")
    Q_PROPERTY (int  amount MEMBER amount)
    Q_PROPERTY (Type type   MEMBER type)

public:
    int amount;
    enum Type { U, S, M, H, D, W, Y } type;
    Q_ENUM (Type)

    static Duration fromString (const QString & arg) { Q_UNUSED (arg) return { }; }
    inline QString toString (void) const { return { }; }

    inline bool operator!= (const Duration & other) const { return (amount != other.amount || type != other.type); }
};
Q_DECLARE_METATYPE (Duration) // needed to be usable in properties

struct DurationTypeDef : public TypeDef<Duration> {
    static cpp_ret_type fallback (void) { return { }; }

    static const QString & sqlType (void) { static const QString ret { QS ("TEXT") }; return ret; }

    static qml_ret_type toQmlJsVar   (cpp_arg_type arg) { return arg; }
    static QJsonValue   toJsonValue  (cpp_arg_type arg) { return arg.toString (); }
    static QVariant     toSqlVariant (cpp_arg_type arg) { return arg.toString (); }

    static cpp_ret_type fromQmlJsVar   (qml_arg_type       arg) { return arg; }
    static cpp_ret_type fromJsonValue  (const QJsonValue & arg) { return Duration::fromString (arg.toString ()); }
    static cpp_ret_type fromSqlVariant (const QVariant &   arg) { return Duration::fromString (arg.toString ()); }
};

template<typename QT_ENUM> struct EnumTypeDef : public TypeDef<QT_ENUM, int> {
    using type_def = TypeDef<QT_ENUM, int>;

    static const QString & sqlType (void) { static const QString ret { QS ("TEXT") }; return ret; }

    static const QMetaEnum & META (void) {
        static const QMetaEnum ret { QMetaEnum::fromType<typename type_def::cpp_ret_type> () };
        return ret;
    }

    static inline QString stringify (typename type_def::cpp_arg_type arg) {
        return QString::fromLatin1 (META ().valueToKey (arg));
    }

    static inline typename type_def::cpp_ret_type parse (const QString & arg) {
        bool ok { false };
        const QByteArray tmp { arg.toLatin1 () };
        const int ret { META ().keyToValue (tmp.constData (), &ok) };
        return typename type_def::cpp_ret_type (ok ? ret : fallback ());
    }

    static typename type_def::cpp_ret_type fallback (void) { return typename type_def::cpp_ret_type (META ().value (0)); }

    static typename type_def::qml_ret_type toQmlJsVar   (typename type_def::cpp_arg_type arg) { return typename type_def::qml_ret_type (arg); }
    static QJsonValue                      toJsonValue  (typename type_def::cpp_arg_type arg) { return stringify (arg); }
    static QVariant                        toSqlVariant (typename type_def::cpp_arg_type arg) { return stringify (arg); }

    static typename type_def::cpp_ret_type fromQmlJsVar   (typename type_def::qml_arg_type arg) { return parse (stringify (typename type_def::cpp_ret_type (arg))); }
    static typename type_def::cpp_ret_type fromJsonValue  (const QJsonValue &              arg) { return parse (arg.toString ()); }
    static typename type_def::cpp_ret_type fromSqlVariant (const QVariant &                arg) { return parse (arg.toString ()); }
};

using CountryTypeDef = EnumTypeDef<QLocale::Country>;
using WeekdayTypeDef = EnumTypeDef<Qt::DayOfWeek>;
