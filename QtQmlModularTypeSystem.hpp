#pragma once

#include <QObject>
#include <QString>
#include <QVariant>
#include <QJsonValue>
#include <QJsonObject>
#include <QMetaProperty>
#include <qqml.h>

#include <type_traits>

#ifndef QS
#define QS QStringLiteral
#endif

template<class T, class QT_OBJ> QList<T *> extractInstancesFromQtProperties (const QT_OBJ * self) {
    QList<T *> ret { };
    const int min { QT_OBJ::staticMetaObject.propertyCount () };
    const int max { self->metaObject ()->propertyCount () };
    for (int idx { min }; idx < max; ++idx) {
       if (T * field { self->metaObject ()->property (idx).read (self).template value<T *> () }) {
            ret.append (field);
       }
    }
    return ret;
}

template<typename CXX_TYPE, typename QML_TYPE = CXX_TYPE> struct TypeDef {
    using cpp_type     = CXX_TYPE;
    using qml_type     = QML_TYPE;
    using cpp_ret_type = CXX_TYPE;
    using cpp_arg_type = std::conditional_t<QTypeInfo<CXX_TYPE>::isComplex, const CXX_TYPE &, const CXX_TYPE>;
    using qml_ret_type = QML_TYPE;
    using qml_arg_type = std::conditional_t<QTypeInfo<QML_TYPE>::isComplex, const QML_TYPE &, const QML_TYPE>;

    static const QString & sqlType (void);

    static cpp_ret_type fallback (void);

    static qml_ret_type toQmlJsVar   (cpp_arg_type arg);
    static QJsonValue   toJsonValue  (cpp_arg_type arg);
    static QVariant     toSqlVariant (cpp_arg_type arg);

    static cpp_ret_type fromQmlJsVar   (qml_arg_type       arg);
    static cpp_ret_type fromJsonValue  (const QJsonValue & arg);
    static cpp_ret_type fromSqlVariant (const QVariant &   arg);
};

template<class TYPE_DEF, typename STRONG_TYPE> class Phantom {
public:
    using type_def    = TYPE_DEF;
    using strong_type = STRONG_TYPE;

    Phantom (void) noexcept : _data { } { }
    Phantom (Phantom && other) noexcept : _data (std::move (other._data)) { }
    Phantom (typename type_def::cpp_type && data) noexcept : _data (std::move (data)) { }
    Phantom (const Phantom & other) noexcept : _data (other._data) { }
    Phantom (const typename type_def::cpp_type & data) noexcept : _data (data) { }

    static strong_type fromJsonValue (const QJsonValue & json) { return strong_type { type_def::fromJsonValue (json) }; }
    inline QJsonValue toJsonValue (void) const { return type_def::toJsonValue (_data); }

    static strong_type fromSqlVariant (const QVariant & var) { return strong_type { type_def::fromSqlVariant (var) }; }
    inline QVariant toSqlVariant (void) const { return type_def::toSqlVariant (_data); }

    static strong_type fromQmlJsVar (typename type_def::qml_arg_type arg) { return strong_type { type_def::fromQmlJsVar (arg) }; }
    inline typename type_def::qml_ret_type toQmlJsVar (void) const { return type_def::toQmlJsVar (_data); }

    inline typename type_def::cpp_type         copyData  (void)       { return _data; }
    inline typename type_def::cpp_type &       refData   (void)       { return _data; }
    inline typename type_def::cpp_type &&      movedData (void)       { return std::move (_data); }
    inline const typename type_def::cpp_type & constData (void) const { return _data; }

private:
    typename type_def::cpp_type _data;
};

struct DataFlag {
    enum Type {
        NORMAL,
        PR_KEY,
        UNIQUE,
        MANDTY,
    };
};

class FieldDef;

class TableDef : public QObject {
    Q_OBJECT
    QML_UNCREATABLE ("C++ constructed class !")
    QML_NAMED_ELEMENT (TableDef)
    Q_PROPERTY (QString           name   READ name    CONSTANT)
    Q_PROPERTY (QString           desc   READ desc    CONSTANT)
    Q_PROPERTY (bool              shared READ shared  CONSTANT)
    Q_PROPERTY (FieldDef *        pkey   READ getPKey CONSTANT)
    Q_PROPERTY (QList<FieldDef *> fields READ fields  CONSTANT)

public:
    explicit TableDef (QString name, QString desc, const bool shared, QObject * parent = nullptr) noexcept
        : QObject { parent }
        , _name   { std::move (name) }
        , _desc   { std::move (desc) }
        , _shared { shared }
    { }

    bool shared (void) const { return _shared; }

    const QString & name (void) const { return _name; }
    const QString & desc (void) const { return _desc; }

    QList<FieldDef *> fields (void) const {
        return extractInstancesFromQtProperties<FieldDef> (this);
    }

    virtual FieldDef * getPKey (void) { return nullptr; }

private:
    const QString _name;
    const QString _desc;
    const bool _shared;
};

class FieldDef : public QObject {
    Q_OBJECT
    QML_UNCREATABLE ("Abstract base class !")
    QML_NAMED_ELEMENT (FieldDef)
    Q_PROPERTY (QString         name     READ name             CONSTANT)
    Q_PROPERTY (QString         typeName READ get_typeName_qml CONSTANT)
    Q_PROPERTY (QStringList     typeLine READ get_typeLine_qml CONSTANT)
    Q_PROPERTY (DataFlag::Type  flag     READ flag             CONSTANT)
    Q_PROPERTY (QString         desc     READ desc             CONSTANT)
    Q_PROPERTY (QString         hint     READ hint             CONSTANT)
    Q_PROPERTY (TableDef *      table    MEMBER _table         CONSTANT)

public:
    explicit FieldDef (QString name, const DataFlag::Type flag, QString desc, QString hint, TableDef * parent = nullptr) noexcept
        : QObject { parent }
        , _name   { std::move (name) }
        , _desc   { std::move (desc) }
        , _hint   { std::move (hint) }
        , _flag   { flag }
        , _table  { parent }
    { }

    const QString & name (void) const { return _name; }
    const QString & desc (void) const { return _desc; }
    const QString & hint (void) const { return _hint; }
    DataFlag::Type  flag (void) const { return _flag; }

    virtual const QString & sqlType (void) const = 0;

    virtual QJsonValue fromSqlVariantToJsonValue (const QVariant   & var)  const = 0;
    virtual QVariant   fromJsonValueToSqlVariant (const QJsonValue & json) const = 0;

    virtual bool isAlias (void) const { return false; }
#ifdef QT_SQL_LIB
    virtual FieldRef * createRef (void) const;
#endif
    virtual QString defaultAlias (void) const { return { }; }

protected:
    virtual const QString     & get_typeName_qml (void) const = 0;
    virtual const QStringList & get_typeLine_qml (void) const = 0;

private:
    const QString _name;
    const QString _desc;
    const QString _hint;
    const DataFlag::Type _flag;
    TableDef * _table;
};

template<typename FIELD_DEF> class FieldAliasDef : public FIELD_DEF {
private:
    const FIELD_DEF & _other;

public:
    explicit FieldAliasDef (QString aliasName, const FIELD_DEF & other, TableDef * parent = nullptr)
        : FIELD_DEF { std::move (aliasName), other.flag (), other.desc (), other.hint (), parent }
        , _other    { other }
    { }

    bool isAlias (void) const final { return true; }
#ifdef QT_SQL_LIB
    virtual FieldRef * createRef (void) const;
#endif
    QString defaultAlias (void) const final { return _other.name (); }
};

class WrapperDef : public QObject {
    Q_OBJECT
    QML_UNCREATABLE ("Abstract base class !")
    QML_NAMED_ELEMENT (WrapperDef)
    Q_PROPERTY (FieldDef * field READ get_field_qml CONSTANT)

public:
    explicit WrapperDef (QObject * parent = nullptr) noexcept : QObject { parent } { }

signals:
    void oldValueChanged (void);
    void newValueChanged (void);

protected:
    virtual FieldDef * get_field_qml (void) const = 0;
};

template<typename FIELDEF> class WrapperImpl : public WrapperDef {
public:
    using field_def_type = FIELDEF;
    using strong_type    = typename FIELDEF::strong_type;

    const FieldDef & field (void) const { return field; }

    explicit WrapperImpl (field_def_type & fieldDef, QObject * parent = nullptr) noexcept
        : WrapperDef { parent }
        , _field     { fieldDef }
        // no { } starting from here to avoid QJsonArray bug :
        , _oldValue  (strong_type::type_def::fallback ())
        , _newValue  (_oldValue)
    { }

    typename strong_type::type_def::qml_ret_type getOldValue (void) const {
        return strong_type::type_def::toQmlJsVar (_oldValue);
    }
    typename strong_type::type_def::qml_ret_type getNewValue (void) const {
        return strong_type::type_def::toQmlJsVar (_newValue);
    }

    void setOldValue (typename strong_type::type_def::qml_arg_type arg) {
        const typename strong_type::type_def::cpp_type tmp = strong_type::type_def::fromQmlJsVar (arg);
        if (tmp != _oldValue) {
            _oldValue = tmp;
            emit oldValueChanged ();
        }
    };
    void setNewValue (typename strong_type::type_def::qml_arg_type arg) {
        const typename strong_type::type_def::cpp_type tmp = strong_type::type_def::fromQmlJsVar (arg);
        if (tmp != _newValue) {
            _newValue = tmp;
            emit newValueChanged ();
        }
    };

private:
    field_def_type & _field;
    typename strong_type::type_def::cpp_type _oldValue;
    typename strong_type::type_def::cpp_type _newValue;

    FieldDef * get_field_qml (void) const final { return &_field; }
};

class AbstractModeldata : public QObject {
    Q_OBJECT
    QML_UNCREATABLE ("Abstract base class !")
    QML_NAMED_ELEMENT (AbstractModeldata)
    Q_PROPERTY (QList<WrapperDef *> wrappersList READ getWrappersList CONSTANT)

public:
    explicit AbstractModeldata (QObject * parent = nullptr) noexcept : QObject { parent } { }

    QList<WrapperDef *> getWrappersList (void) const {
        return extractInstancesFromQtProperties<WrapperDef> (this);
    }
};

class JsonObject {
public:
    struct KeyVal {
        const QString    key;
        const QJsonValue val;

        template<typename T> KeyVal (const T & field, const typename T::strong_type & arg, const bool condition = true)
            : KeyVal { field.name (), (condition ? arg.toJsonValue () : QJsonValue::Undefined) }
        { }

    private:
        explicit KeyVal (QString key, QJsonValue val) : key { std::move (key) }, val { std::move (val) } { }

        friend class JsonObject;
    };

    JsonObject (void) noexcept { }
    JsonObject (JsonObject && other) noexcept : _data { std::move (other._data) } { }
    JsonObject (QJsonObject && json) noexcept : _data { std::move (json) } { }
    JsonObject (const JsonObject & other) noexcept : _data { other._data } { }
    JsonObject (const QJsonObject & json) noexcept : _data { json } { }
    JsonObject (std::initializer_list<KeyVal> list) noexcept {
        for (const KeyVal & pair : list) {
            _data.insert (pair.key, pair.val);
        }
    }

    inline void clear (void) {
        _data = { };
    }

    inline int count (void) const {
        return _data.count ();
    }

    inline bool isEmpty (void) const {
        return _data.isEmpty ();
    }

    template<typename T> inline bool has (const T & field) const {
        return _data.contains (field.name ());
    }

    inline QJsonValue get (const TableDef & table) const {
        return _data.value (table.name ());
    }

    template<typename T> inline typename T::strong_type get (const T & field) const {
        return T::strong_type::type_def::fromJsonValue (_data.value (field.name ()));
    }

    template<typename T> inline void set (const T & field, const typename T::strong_type & arg) {
        _data.insert (field.name (), arg.toJsonValue ());
    }

    template<typename T> inline void unset (const T & field) {
        _data.remove (field.name ());
    }

    struct iterator {
        QJsonObject::const_iterator _it;

        explicit iterator (QJsonObject::const_iterator it) : _it { it } { }

        inline bool operator== (const iterator & other) const { return (_it == other._it); }
        inline bool operator!= (const iterator & other) const { return (_it != other._it); }
        inline iterator & operator++ (void) { ++_it; return (*this); }
        inline KeyVal operator* (void) const { return KeyVal { this->_it.key (), this->_it.value () }; }
    };
    inline iterator begin (void) const { return iterator { _data.constBegin () }; }
    inline iterator end   (void) const { return iterator { _data.constEnd   () }; }

    inline operator QJsonObject (void) const { return _data; }

private:
    QJsonObject _data;
};

#define QML_DECLARE_STRONG_TYPE(NAME, QML_TYPE) \
    class NAME##Data : public Phantom<NAME##TypeDef, NAME##Data> { \
    public: using Phantom::Phantom; \
    }; \
    class NAME##Wrapper; \
    class NAME##Field : public FieldDef { \
    private: Q_OBJECT \
    private: QML_NAMED_ELEMENT (NAME##Field) \
    public: using strong_type = NAME##Data; \
    public: using wrapper_type = NAME##Wrapper; \
    public: static const QString     & typeName (void) { static const QString     ret { QS (#NAME) }; return ret; } \
    public: static const QStringList & typeLine (void) { static const QStringList ret { QS (#NAME) }; return ret; } \
    public: const QString & sqlType (void) const final { return strong_type::type_def::sqlType (); } \
    public: QJsonValue fromSqlVariantToJsonValue (const QVariant & var) const final { \
    strong_type::type_def::cpp_type tmp { strong_type::type_def::fromSqlVariant (var) }; \
    return (tmp != strong_type::type_def::fallback () ? strong_type::type_def::toJsonValue (tmp) : QJsonValue::Undefined); \
    } \
    public: QVariant fromJsonValueToSqlVariant (const QJsonValue & json) const final { return strong_type::fromJsonValue (json).toSqlVariant (); } \
    protected: const QString     & get_typeName_qml (void) const override { return NAME##Field::typeName (); } \
    protected: const QStringList & get_typeLine_qml (void) const override { return NAME##Field::typeLine (); } \
    public: explicit NAME##Field (QString name, const DataFlag::Type flag, QString desc, QString hint, TableDef * parent = nullptr) noexcept \
    : FieldDef { std::move (name), flag, std::move (desc), std::move (hint), parent } { } \
    }; \
    class NAME##Wrapper : public WrapperImpl<NAME##Field> { \
    static_assert (std::is_same_v<QML_TYPE, NAME##TypeDef::qml_type>, "The QML_TYPE '" #QML_TYPE "' differs from '" #NAME "TypeDef::qml_type' !"); \
    private: Q_OBJECT \
    private: QML_NAMED_ELEMENT (NAME##Wrapper) \
    private: Q_PROPERTY (QML_TYPE oldValue READ getOldValue                   NOTIFY oldValueChanged) \
    private: Q_PROPERTY (QML_TYPE newValue READ getNewValue WRITE setNewValue NOTIFY newValueChanged) \
    public: using WrapperImpl::WrapperImpl; \
    };

#define QML_DERIVED_STRONG_TYPE(NAME, ANCESTOR) \
    class NAME##Data : public ANCESTOR##Data { \
    public: using ANCESTOR##Data::ANCESTOR##Data; \
    }; \
    class NAME##Wrapper; \
    class NAME##Field : public ANCESTOR##Field { \
    private: Q_OBJECT \
    private: QML_NAMED_ELEMENT (NAME##Field) \
    public: static const QString     & typeName (void) { static const QString     ret { QS (#NAME) }; return ret; } \
    public: static const QStringList & typeLine (void) { static const QStringList ret { ANCESTOR##Field::typeLine () + QStringList { QS (#NAME) } }; return ret; } \
    protected: const QString     & get_typeName_qml (void) const override { return NAME##Field::typeName (); } \
    protected: const QStringList & get_typeLine_qml (void) const override { return NAME##Field::typeLine (); } \
    public: using strong_type = NAME##Data; \
    public: using wrapper_type = NAME##Wrapper; \
    public: explicit NAME##Field (QString name, const DataFlag::Type flag, QString desc, QString hint, TableDef * parent = nullptr) noexcept \
    : ANCESTOR##Field { std::move (name), flag, std::move (desc), std::move (hint), parent } { } \
    }; \
    class NAME##Wrapper : public ANCESTOR##Wrapper { \
    private: Q_OBJECT \
    private: QML_NAMED_ELEMENT (NAME##Wrapper) \
    public: explicit NAME##Wrapper (NAME##Field & field, QObject * parent = nullptr) noexcept : ANCESTOR##Wrapper { field, parent } { } \
    };

#define QML_FIELD_DEF(NAME, TYPE, FLAG, DESC, HINT) \
    public: TYPE##Field NAME { QS (#NAME), FLAG, QS (DESC), QS (HINT), this }; \
    public: FieldDef * get_##NAME##_qml (void) { return &(NAME); } \
    private: Q_PROPERTY (FieldDef * NAME READ get_##NAME##_qml CONSTANT)

#define QML_GUID_DEF(NAME, TYPE) \
    private: QML_FIELD_DEF (NAME, TYPE, DataFlag::PR_KEY, "idenfiant unique", " ") \
    public: FieldDef * getPKey (void) final { return &(NAME); }

#define QML_ALIAS_DEF(NAME, FIELD) \
    public: FieldAliasDef<decltype (FIELD)> NAME { QS (#NAME), FIELD, this }; \
    public: FieldDef * get_##NAME##_qml (void) { return &(NAME); } \
    private: Q_PROPERTY (FieldDef * NAME READ get_##NAME##_qml CONSTANT)

#define QML_DATA_WRAPPER(NAME, FIELD_DEF) \
    public: decltype (FIELD_DEF)::wrapper_type NAME { FIELD_DEF, this }; \
    public: WrapperDef * get_##NAME##_qml (void) { return &(NAME); } \
    private: Q_PROPERTY (WrapperDef * NAME READ get_##NAME##_qml CONSTANT)

#define QML_TABLE_DEF(NAME, TYPE, SHARED, DESC) \
    public:  TYPE NAME { QS (#NAME), QS (DESC), SHARED, this }; \
    public:  TYPE * get_##NAME##_qml (void) { return &(NAME); } \
    private: Q_PROPERTY (TYPE * NAME READ get_##NAME##_qml CONSTANT)
