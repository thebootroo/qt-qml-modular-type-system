
#include "main.hpp"

static_assert (std::is_same_v<StringTypeDef::qml_arg_type, const QString &>, "nope");

int main (int argc, char * argv []) {

    StringData teststr = GuidData { QS ("toto") };

    GuidField * testfield1 { new GuidField { QS ("tata"), DataFlag::NORMAL, { }, { } } };
    StringField * testfield2 { testfield1 };

    StringWrapper * testwrapper { new GuidWrapper { *testfield1 } };

    Q_UNUSED (testfield2)
    Q_UNUSED (testwrapper)

    TestModeldata test { };
    test.firstname.setOldValue ("foo");
    test.email.setNewValue ("foo@bar.net");
    test.nationality.setNewValue (QLocale::France);

    static_assert (std::is_same_v<GuidWrapper::strong_type, StringData>, "nope");

    //qRegisterMetaType<Duration> (); // gadget

    /// register those in different module as one can't mix QML_ELEMENT and qmlRegister*** in same module
    qmlRegisterSingletonInstance<SampleDatabase> ("MyTestApp", 1, 0, "BDD", &DB);
    qmlRegisterSingletonInstance<TestModeldata>  ("MyTestApp", 1, 0, "TST", &test);

    /// start the test app :
    QGuiApplication app { argc, argv };
    QQmlApplicationEngine engine {
        QUrl { QS ("qrc:///ui.qml") }
    };

    BooleanField FAKE_NOMAIL { QS ("no_mail"), DataFlag::NORMAL, { }, { } };

    JsonObject json {
        { DB.contact_firstname, StringData { "toto" } },
        { DB.contact_guid, GuidData { "tata" } },
        { DB.contact_size, IntegerData { 42 }, false }, // this key will be excluded
        { FAKE_NOMAIL, BooleanData { true } },
    };

    qInfo () << "json" << json;

    for (const JsonObject::KeyVal & pair : qAsConst (json)) {
        qInfo () << pair.key << "->" << pair.val;
    }

    qInfo () << "before" << json.get (DB.contact_firstname).constData ();
    json.set (DB.contact_firstname,  StringData { QS ("tata") });
    qInfo () << "after" << json.get (DB.contact_firstname).constData ();

    qDebug () << "contains" << json.has (DB.contact_firstname);

    StringData str { QS ("foobar") };
    DateTimeData dt { };
    IntegerData num { 123 };
    DecimalData val { 456.789 };
    BooleanData flag { true };

    qInfo () << "Dynamic enum from js number" << CountryData::fromQmlJsVar (74).copyData ();
    qInfo () << "Dynamic enum from json string" << WeekdayData::fromJsonValue ("Friday").copyData ();

    return QGuiApplication::exec ();
}
