#pragma once

#include <QObject>

#include "MyTypeDefs.hpp"

/// declare the database fields once, with a link between name and type :

class SampleDatabase : public TableDef {
    Q_OBJECT
    QML_GUID_DEF  (contact_guid,        Guid)
    QML_FIELD_DEF (contact_firstname,   String,   DataFlag::NORMAL, " ", " ")
    QML_FIELD_DEF (contact_lastname,    String,   DataFlag::NORMAL, " ", " ")
    QML_FIELD_DEF (contact_emailAddr,   Email,    DataFlag::NORMAL, " ", " ")
    QML_FIELD_DEF (contact_phoneNum,    Phone,    DataFlag::NORMAL, " ", " ")
    QML_FIELD_DEF (contact_birthday,    DateTime, DataFlag::NORMAL, " ", " ")
    QML_FIELD_DEF (contact_weight,      Decimal,  DataFlag::NORMAL, " ", " ")
    QML_FIELD_DEF (contact_size,        Integer,  DataFlag::NORMAL, " ", " ")
    QML_FIELD_DEF (contact_nationality, Country,  DataFlag::NORMAL, " ", " ")
    QML_FIELD_DEF (contact_nicknames,   List,     DataFlag::NORMAL, " ", " ")
    QML_FIELD_DEF (contact_seniority,   Duration, DataFlag::NORMAL, " ", " ")
    QML_ALIAS_DEF (contact_country,     contact_nationality)

public:
    static SampleDatabase & instance (void) {
        static SampleDatabase ret { };
        return ret;
    }

private:
    explicit SampleDatabase (QObject * parent = nullptr) : TableDef { QS ("Sample"), QS ("an example"), false, parent } { };
};

#define DB SampleDatabase::instance ()
