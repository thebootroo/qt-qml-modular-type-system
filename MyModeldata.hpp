#pragma once

#include <QObject>

#include "MyDataBase.hpp"

/// define the data object that will be use by QML :

class TestModeldata : public AbstractModeldata {
    Q_OBJECT
    QML_DATA_WRAPPER (guid,        DB.contact_guid)
    QML_DATA_WRAPPER (firstname,   DB.contact_firstname)
    QML_DATA_WRAPPER (lastname,    DB.contact_lastname)
    QML_DATA_WRAPPER (email,       DB.contact_emailAddr)
    QML_DATA_WRAPPER (birthday,    DB.contact_birthday)
    QML_DATA_WRAPPER (nationality, DB.contact_nationality)
    QML_DATA_WRAPPER (nicknames,   DB.contact_nicknames)
    QML_DATA_WRAPPER (seniority,   DB.contact_seniority)

public:
    using AbstractModeldata::AbstractModeldata;
};
